A simple pinball. But you control the ball.

Use the arrow keys.

# Running from source

You need python (you already have them on linux)

from the root of the package (you might have to use python3 in place of python)

    # create the virtual environement
    python -m venv venv
    # activate it
    . .\venv\Scripts\activate
    # install arcade
    python -m pip install arcade
    # run the game
    python mygame.py

# Build it
You can build a standalone version for Windows by running the build.bat script.
