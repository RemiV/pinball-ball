import math
import os
import sys
import arcade
from arcade.application import Window
import arcade.tilemap
import arcade.color
import arcade.key
from pyglet.math import Mat4

import pymunk

HEIGHT = 32*18
WIDTH = 32*20
FLIPPER_WIDTH=32*2+16
SPRITE_WIDTH=32
DECALAGE = FLIPPER_WIDTH/2 - SPRITE_WIDTH/2


GRAVITY = 1500
DEFAULT_DAMPING = 1.0
PLAYER_DAMPING = 1

PLAYER_FRICTION = 0.9
WALL_FRICTION = 0.7
DYNAMIC_ITEM_FRICTION = 0.6

# Mass (defaults to 1)
PLAYER_MASS = .4
ACTION_FORCE = 700
ACTION_IMPULSE = 400


# Keep player from going too fast
PLAYER_MAX_SPEED = 1600

SPRITES_FILE = "assets/sprites/sprites.png"
SOUND_PATH = "assets/sound/"
HEART_TEXTURE = 50


class MyGame(arcade.Window):
    def __init__(self):
        """ Initializer """
        # Call the parent class initializer
        super().__init__(WIDTH, HEIGHT, "Pinball: the ball") # type: ignore
        arcade.set_background_color(arcade.color.AERO_BLUE)


    def start_game(self):
        view = PinBall(self)
        view.setup()
        self.show_view(view)

    def start_menu(self, cur_score=None, top_score=None):
        if top_score is None and os.path.isfile("score"):
            with open("score") as f:
                score = f.readline()
                top_score = int(score)
        elif top_score is None:
            top_score = 0

        if cur_score is not None and cur_score > top_score:
            top_score = cur_score

        with open("score", "w") as f:
            f.write(str(top_score))
        
        view = Menu(self, cur_score, top_score)
        view.setup()
        self.show_view(view)


class Menu(arcade.View):
    tile_map: arcade.TileMap
    scene: arcade.Scene
    left:float
    right:float
    top_score:int
    cur_score:int|None

    def __init__(self, game:MyGame, cur_score:int|None, top_score:int):
        super().__init__(game)
        self.game = game
        self.cur_score = cur_score
        self.top_score = top_score


    def setup(self):
        self.map = arcade.load_tilemap("assets/maps/menu.tmj")
        self.scene = arcade.Scene.from_tilemap(self.map)
        self.texts = []
        self.rects = []
        self.drawable_rect = []

        for obj in self.map.object_lists["text"]:
            if obj.name == "left":
                self.left = obj.shape[0]
            elif obj.name == "right":
                self.right = obj.shape[0]
        width = self.right - self.left
        for obj in self.map.object_lists["text"]:        
            if obj.properties and "text" in obj.properties:
                txt = obj.properties["text"]
                x, y = obj.shape
                assert isinstance(x, float)
                assert isinstance(y, float)
                self.add_txt(width, txt, x, y)
            if obj.properties and "action" in obj.properties:
                x, y = obj.shape
                assert isinstance(x, float)
                assert isinstance(y, float)
                rect = arcade.create_rectangle(x, y+3, width, SPRITE_WIDTH, color=arcade.color.BLUE_GREEN)
                self.rects.append(rect)
                rect.action = obj.properties["action"]
                rect.dimention = (x - width/2, y + 3-SPRITE_WIDTH/2, x + width/2, y + 3 + SPRITE_WIDTH/2)
            
            if obj.name == "high-score":
                x, y = obj.shape
                assert isinstance(x, float)
                assert isinstance(y, float)
                if self.cur_score is None:
                    txt = f"high score: {self.top_score:011,}"
                else:
                    txt = f"high score: {self.top_score:011,}\nlast score: {self.cur_score:011,}"
                self.add_txt(width, txt, x, y)

    def add_txt(self, width, txt, x, y):
        self.texts.append(
                    arcade.Text(txt, x, y+3,
                                width=int(width), multiline = True,
                                color=arcade.color.BLACK_OLIVE,
                                align="center",
                                anchor_x="center",
                                anchor_y="center"))

        

    def on_draw(self):
        self.clear()
        #self.set_angle()
 
        self.scene.draw()
        for rect in self.drawable_rect:
            rect.draw()
        for txt in self.texts:
            txt.draw()
    
    def on_mouse_motion(self, x: int, y: int, dx: int, dy: int):
        self.drawable_rect = []
        rect:arcade.Shape
        for rect in self.rects:
            l, b, r, t = rect.dimention
            if l < x < r and b < y < t:
                self.drawable_rect.append(rect)
                
        return super().on_mouse_motion(x, y, dx, dy)
    
    def on_mouse_press(self, x: int, y: int, button: int, modifiers: int):
        for rect in self.rects:
            l, b, r, t = rect.dimention
            if l < x < r and b < y < t:
                action = rect.action
                if action == "quit":
                    arcade.exit()
                if action == "start":
                    self.game.start_game()
        return super().on_mouse_press(x, y, button, modifiers)


class PinBall(arcade.View):
    tile_map: arcade.TileMap
    scene: arcade.Scene
    player: arcade.Sprite
    player_body: pymunk.Body
    right_flipper: arcade.Sprite
    left_axe: tuple[float, float]
    left_body: pymunk.Body
    right_flipper: arcade.Sprite
    right_axe: tuple[float, float]
    right_body: pymunk.Body
    physics_engine: arcade.PymunkPhysicsEngine

    score = 0

    left_active = False
    right_active = False
    launcher_active = False
    lspeed = 0.
    angle: float = 0.
    new_angle: float = 0.
    ia_waiting: bool|int = False

    active_key = set()

    zone_height: float
    zone_left: float
    zone_right: float
    zone_centre: float

    def __init__(self, game:MyGame):
        super().__init__(game)
        self.game = game

    def setup(self):
        layer_options = {
            "Platforms": {
                "use_spatial_hash": True,
            },
        }
        self.map = arcade.load_tilemap("assets/maps/snd-level.tmj", layer_options=layer_options)
        self.scene = arcade.Scene.from_tilemap(self.map)

        self.setup_objects()
        self.setup_physics()
        self.setup_zone()
        self.setup_camera()
        self.setup_sound()


    def setup_physics(self):
        self.physics_engine = arcade.PymunkPhysicsEngine(damping=DEFAULT_DAMPING,
                                                         gravity=(0, -GRAVITY))

        self.physics_engine.add_sprite(self.player,
                                       elasticity=0.9,
                                       friction=PLAYER_FRICTION,
                                       mass=PLAYER_MASS,
                                       collision_type="player",
                                       max_horizontal_velocity=PLAYER_MAX_SPEED,
                                       max_vertical_velocity=PLAYER_MAX_SPEED,
                                       radius = 0.5)
        
        body = self.physics_engine.sprites[self.player].body
        assert body
        self.player_body = body

        self.physics_engine.add_sprite_list(self.scene.get_sprite_list("wall"),
                                            elasticity=0.5,
                                            friction=WALL_FRICTION,
                                            collision_type="wall",
                                            body_type=arcade.PymunkPhysicsEngine.STATIC)

        self.physics_engine.add_sprite_list(self.scene.get_sprite_list("bumper"),
                                            elasticity=2,
                                            friction=WALL_FRICTION,
                                            collision_type="bumper",
                                            body_type=arcade.PymunkPhysicsEngine.STATIC)

        self.physics_engine.add_sprite(self.launcher,
                                       elasticity=0.5,
                                       friction=WALL_FRICTION,
                                       body_type=arcade.PymunkPhysicsEngine.KINEMATIC)

        self.physics_engine.add_sprite_list(self.scene.get_sprite_list("flipper"),
                                            elasticity=0.4,
                                            friction=WALL_FRICTION,
                                            collision_type="flipper ",
                                            body_type=arcade.PymunkPhysicsEngine.KINEMATIC)


        self.physics_engine.add_collision_handler("bumper", "player", post_handler=self.on_bumper_hit)
        self.physics_engine.add_collision_handler("wall", "player", post_handler=self.on_wall_hit)

        self.left_body = self.physics_engine.sprites[self.left_flipper].body # type: ignore
        assert self.left_body is not None
        self.right_body = self.physics_engine.sprites[self.right_flipper].body # type: ignore
        assert self.right_body is not None

        self.left_body.center_of_gravity = (-DECALAGE, 0)
        self.left_body.position += (0, 0)

        self.right_body.center_of_gravity = (DECALAGE, 0)
        self.right_body.position += (0, 0)


    def setup_objects(self):
        objs = { obj.name: obj for obj in self.map.object_lists["moving stuff"]}

        textures = arcade.load_spritesheet(SPRITES_FILE, SPRITE_WIDTH, SPRITE_WIDTH, 10, 100)
        self.textures = textures

        self.setup_ball(objs, textures)
        self.setup_flipper(objs["left-flipper"], objs["right-flipper"])
        self.setup_launcher()
        self.setup_heart(textures, objs["life"])
        self.setup_score(objs["score"])
        self.setup_target(textures)


    def setup_flipper(self, left:arcade.TiledObject, right:arcade.TiledObject):
        self.left_flipper = arcade.Sprite(SPRITES_FILE, image_x=0, image_y=64, image_width=FLIPPER_WIDTH, image_height=SPRITE_WIDTH, 
                                          center_x=left.shape[0] + DECALAGE, center_y=left.shape[1], flipped_horizontally=False) # type: ignore
        self.left_axe = left.shape # type: ignore
        self.scene.add_sprite("flipper", self.left_flipper)
        self.rotate_sprite_around_point(self.left_flipper, self.left_axe, -10 ) # type: ignore

        self.right_flipper = arcade.Sprite(SPRITES_FILE, image_x=0, image_y=64, image_width=FLIPPER_WIDTH, image_height=SPRITE_WIDTH, 
                                          center_x=right.shape[0] - DECALAGE, center_y=right.shape[1], flipped_horizontally=True) # type: ignore
        self.right_axe = right.shape # type: ignore
        self.scene.add_sprite("flipper", self.right_flipper)
        self.rotate_sprite_around_point(self.right_flipper, self.right_axe, 10 ) # type: ignore


    def setup_score(self, obj:arcade.TiledObject):
        self.score_text = arcade.Text("Score: \n 000 000", start_x=obj.shape[0], start_y=obj.shape[1], # type: ignore
                                      color=arcade.color.BLACK, font_size=20,
                                      width=SPRITE_WIDTH * 4, multiline = True)


    def setup_heart(self, textures:list[arcade.Texture], obj:arcade.TiledObject):
        for i in range(3):
            heart = arcade.Sprite(texture=textures[HEART_TEXTURE], center_x=obj.shape[0] + SPRITE_WIDTH/2 + i * (SPRITE_WIDTH + 1), center_y=obj.shape[1]) # type: ignore
            heart.alive = True # type: ignore
            self.scene.add_sprite("life", heart)


    def setup_launcher(self):
        launchers = self.scene.get_sprite_list("launcher")
        center = launchers.center
        launchers.clear()
        self.launcher = arcade.Sprite(SPRITES_FILE,
                                      image_x=SPRITE_WIDTH * 1, image_y=SPRITE_WIDTH * 3,
                                      image_width=SPRITE_WIDTH, image_height=SPRITE_WIDTH * 2, 
                                      center_x=center[0] - 3, center_y=center[1]) # type: ignore
        self.scene.add_sprite("launcer", self.launcher)


    def setup_ball(self, objs, textures):
        obj = objs["ball"]
        self.init_pos = (obj.shape[0], obj.shape[1])
        self.player = arcade.Sprite(texture=textures[0],
                                    center_x=obj.shape[0], center_y=obj.shape[1]) # type: ignore
        self.scene.add_sprite("player", self.player)

    
    def setup_target(self, textures:list[arcade.Texture]):
        self.target = {}
        for target in self.scene.get_sprite_list("target"):
            target.textures.append(textures[2])
            if target.properties["type"] not in self.target:
                self.target[target.properties["type"]] = 1


    def setup_sound(self):
        self.nop_sound = arcade.Sound(SOUND_PATH + "nop-sound.mp3")
        self.ok_sound = arcade.Sound(SOUND_PATH + "ok-sound.mp3")
        self.simple_tak = arcade.Sound(SOUND_PATH + "simple-tak.mp3")
        self.sound_player = None 

    def play_sound(self, sound:arcade.Sound):
        def reset():
            self.sound_player = None
        if self.sound_player is None or not self.sound_player.playing:
            self.sound_player = sound.play()
            self.sound_player.on_player_eos = reset
    
    def setup_zone(self):
        objs = { obj.name: obj for obj in self.map.object_lists["zone"]}
        self.zone_height = objs["centre"].shape[1] # type: ignore
        self.zone_left = objs["left"].shape[0] # type: ignore
        self.zone_centre = objs["centre"].shape[0] # type: ignore
        self.zone_right = objs["right"].shape[0] # type: ignore


    def setup_camera(self):
        self.camera = arcade.Camera()


    def restart_ball(self):
        self.set_body_position(self.player, (self.init_pos))
        self.set_body_velocity(self.player, (0, 0))
        for heart in self.scene.get_sprite_list("life"):
            if heart.alive: # type: ignore
                heart.alive = False # type: ignore
                heart.texture = self.textures[HEART_TEXTURE + 1]
                return
            
        self.game.start_menu(cur_score=self.score)


    def rotate_sprite_around_point(self, sprite: arcade.Sprite, point: arcade.Point, degrees: float):

        """
        Rotate the sprite around a point by the set amount of degrees

        :param point: The point that the sprite will rotate about
        :param degrees: How many degrees to rotate the sprite
        """
        # Make the sprite turn as its position is moved
        sprite.angle += degrees

        # Move the sprite along a circle centered around the passed point
        sprite.position = arcade.rotate_point(
            sprite.center_x, sprite.center_y,
            point[0], point[1], degrees)


    def apply_force(self, x, y):
        self.player_body.apply_force_at_world_point((x,y), (self.player.center_x, self.player.center_y))

    def apply_impulse(self, x, y):
        self.player_body.apply_impulse_at_world_point((x,y), (self.player.center_x, self.player.center_y))


    def on_key_press(self, symbol: int, modifiers: int):
        if symbol in [arcade.key.LEFT, arcade.key.RIGHT, arcade.key.DOWN, arcade.key.UP,
                      arcade.key.W, arcade.key.A, arcade.key.S, arcade.key.D]:
            self.active_key.add(symbol)
        if symbol == arcade.key.UP or symbol == arcade.key.W:
            self.apply_impulse(0, ACTION_IMPULSE)
        if symbol == arcade.key.DOWN or symbol == arcade.key.S:
            self.launcher_active = True

    def on_key_release(self, symbol: int, modifiers: int):

        if symbol in [arcade.key.LEFT, arcade.key.RIGHT, arcade.key.DOWN, arcade.key.UP,
                      arcade.key.W, arcade.key.A, arcade.key.S, arcade.key.D]:
            if symbol in self.active_key:
                self.active_key.remove(symbol)
        if symbol == arcade.key.DOWN or symbol == arcade.key.S:
            self.launcher_active = False

    
    def on_bumper_hit(self, _bumper, _player, arbiter: pymunk.Arbiter, _space, _data):
        if arbiter.is_first_contact:
            self.score += 100
            self.play_sound(self.simple_tak)


    def on_wall_hit(self, _wall:arcade.Sprite, player:arcade.Sprite, arbiter: pymunk.Arbiter, _space, _data):
        if not arbiter.is_first_contact:
            return
        sound = self.simple_tak
        for target in self.scene.get_sprite_list("target"):
            if target.bottom < player.center_y < target.top and target.left < player.center_x < target.right:
                if self.target[type:=target.properties["type"]] == (rank:=target.properties["rank"]):
                    target.texture = target.textures[1]
                    self.score += rank * rank * 500
                    self.target[type] += 1
                    sound = self.ok_sound
                else:
                    self.reinit_target(type)
                    sound = self.nop_sound
                break
        self.play_sound(sound)

    
    def reinit_target(self, type):
        self.target[type] = 1
        for target in self.scene.get_sprite_list("target"):
            if target.properties["type"] == type:
                target.texture = target.textures[0]
                

    def update_flipper(self, flipper:arcade.Sprite, body:pymunk.Body, active:bool, sign:int):
        angle = flipper.angle * sign

        if active:
            if angle < 35 - 2 * 4:
                mult = 15
            elif angle < 35:
                mult = 15/8 * (35 - angle)
            elif angle < 37:
                mult = 2 * (35 - angle)
            else:
                mult = -4
        else:
            if angle < -37:
                mult = 2
            elif angle < -35.5:
                mult = 1 * (-35 - angle)
                assert(mult >= 0)
            elif angle < -34.5:
                mult = 0
            elif angle < -33:
                mult = 1 * (-35 - angle)
                assert(mult <= 0)
            else:
                mult = -2
        
        body.angular_velocity = mult * sign


    def update_launcher(self):
        if self.launcher_active:
            self.lspeed = 0
            if self.launcher.center_y > 0:
                self.set_body_velocity(self.launcher, [0., -100.])
            else:
                self.set_body_velocity(self.launcher, [0., 0.])
        elif self.launcher.center_y > 32:
            self.set_body_position(self.launcher, [self.launcher.center_x, 32])
            self.launcher.center_y = 32
            self.set_body_velocity(self.launcher, [0., 0.])
        elif self.launcher.center_y == 32:
            self.set_body_velocity(self.launcher, [0, 0])
        else:
            self.lspeed += 600
            self.set_body_velocity(self.launcher, [0, self.lspeed])

    
    def update_ball(self):
        if self.player.center_y < 0:
            self.restart_ball()
        for acc in self.scene.get_sprite_list("accelerator"):
            if arcade.is_point_in_polygon(self.player.center_x - acc.center_x, self.player.center_y - acc.center_y, acc.hit_box):
                self.apply_force(-2 * ACTION_FORCE, 3)

        for symbol in self.active_key:
            if symbol == arcade.key.LEFT or symbol == arcade.key.A:
                self.apply_force(-ACTION_FORCE, 0)
            elif symbol == arcade.key.RIGHT or symbol == arcade.key.D:
                self.apply_force(ACTION_FORCE, 0)
            elif symbol == arcade.key.DOWN or symbol == arcade.key.S:
                self.apply_force(0, -ACTION_FORCE)
            elif symbol == arcade.key.UP or symbol == arcade.key.W:
                self.apply_force(0, -ACTION_FORCE)

    def ia_player(self):
        if self.ia_waiting:
            self.ia_waiting -= 1
        if self.player.center_y < self.zone_height and not self.ia_waiting:
            if math.hypot(*self.get_body_velocity(self.player)) < 50:
                self.ia_waiting = 20
                self.left_active = False
                self.right_active = False
                return
            if self.zone_left < self.player.center_x < self.zone_centre:
                self.left_active = True
                self.right_active = False
            elif self.player.center_x < self.zone_right:
                self.right_active = True
                self.left_active = False
        else:
            self.left_active = False
            self.right_active = False

    def set_body_velocity(self, sprite, v):
        self.physics_engine.get_physics_object(sprite).body.velocity = v  # type: ignore


    def set_body_position(self, sprite:arcade.Sprite, pos):
        self.physics_engine.get_physics_object(sprite).body.position = pos  # type: ignore


    def get_body_velocity(self, sprite:arcade.Sprite):
        return self.physics_engine.get_physics_object(sprite).body.velocity  # type: ignore


    def on_update(self, delta_time: float):
        self.update_flipper(self.left_flipper, self.left_body, self.left_active, 1)
        self.update_flipper(self.right_flipper, self.right_body, self.right_active, -1)
        self.update_launcher()
        self.update_ball()
        self.ia_player()
        self.physics_engine.step()
        self.score_text.text = f"Score: \n{self.score:011,}"
        #self.rotate_sprite_around_point(self.right_flipper,self.left_axe, 10 )


    def on_draw(self):
        self.clear()
        self.camera.use()
        #self.set_angle()
 
        self.scene.draw()
        self.score_text.draw()


    def set_angle(self):
        velocity = self.get_body_velocity(self.player)
        if math.hypot(velocity[1], velocity[0]) > 200:
            self.new_angle = math.atan2(velocity[1], velocity[0])

        while self.angle > self.new_angle + math.pi:
            self.angle -= 2 * math.pi

        while self.angle < self.new_angle - math.pi:
            self.angle += 2 * math.pi
        
        self.angle = 0.99 * self.angle + 0.01 * self.new_angle
   
        self.camera.view_matrix = Mat4.from_rotation(-self.angle, (0, 0, 1)) # type: ignore
        self.camera._window.ctx.projection_2d_matrix = self.camera.projection_matrix @ self.camera.view_matrix # type: ignore



def main():
    """ Main method """
    window = MyGame()
    window.start_menu()
    arcade.run()


if __name__ == "__main__":
    main()